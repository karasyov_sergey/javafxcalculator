package sample.animation;


import javafx.animation.TranslateTransition;
import javafx.scene.Node;
import javafx.util.Duration;

/**
 * Класс, описывающий анимацию управляющего элемента
 *
 * @autor KSO 17ИТ17
 */
public class Shake {
    private TranslateTransition transition;

    public Shake(Node node) {
        transition = new TranslateTransition(Duration.millis(50), node);
        transition.setFromX(-10);
        transition.setByX(10);
        transition.setCycleCount(3);
        transition.setAutoReverse(true);
    }

    public void playAnimation() {
        transition.playFromStart();
    }
}


package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import sample.animation.Shake;

import java.math.BigDecimal;

@SuppressWarnings("unused")
public class Controller {
    @FXML
    public TextField expression;
    @FXML
    public Button buttonResult;
    @FXML
    public Button buttonAllClean;
    @FXML
    public Button buttonClean;
    @FXML
    public Button buttonNegative;
    @FXML
    public Button buttonDouble;

    private static final String ERROR = "Ошибка";

    /**
     * Метод для очистки поля при значениях "Ошибка" или "Infinity"
     */
    private void cleanError() {
        if (expression.getText().equals(ERROR) || expression.getText().equals("Infinity")) {
            expression.clear();
        }
    }

    /**
     * Метод для изменения текстового поля @code{expression} результатом вычислений
     *
     * @param actionEvent событие
     */
    public void printResult(ActionEvent actionEvent) {
        Shake error = new Shake(expression);
        if (expression.getText().isEmpty()) {
            expression.setText(ERROR);
            error.playAnimation();
            return;
        }
        String[] elementsOfExpressions = expression.getText().split(" ");
        if (elementsOfExpressions.length < 3) {
            expression.setText(ERROR);
            error.playAnimation();
            return;
        }
        expression.setText(calculation(elementsOfExpressions));

        returnValueDouble();
    }

    /**
     * Метод для решения числового примера
     *
     * @param elementsOfExpressions массив, содержащий элементы выражения
     * @return результат вычислений
     */
    private String calculation(String[] elementsOfExpressions) {
        BigDecimal firstOperand = BigDecimal.valueOf(Double.parseDouble(elementsOfExpressions[0]));
        String sign = elementsOfExpressions[1];
        BigDecimal secondOperand = BigDecimal.valueOf(Double.parseDouble(elementsOfExpressions[2]));
        switch (sign) {
            case "+":
                return firstOperand.add(secondOperand).toString();
            case "-":
                return firstOperand.subtract(secondOperand).toString();

            case "*":
                return firstOperand.multiply(secondOperand).toString();
            case "/":
                if (secondOperand.equals(BigDecimal.valueOf(0.0))) {
                    return ERROR;
                }
                return firstOperand.divide(secondOperand, 0).toString();
            case "^":
                return String.valueOf(Math.pow(Double.parseDouble(firstOperand.toString()), Double.parseDouble(secondOperand.toString())));

        }
        return ERROR;
    }

    /**
     * Метод для очищения текстового поля @code{expression}
     *
     * @param actionEvent событие
     */
    public void allClean(ActionEvent actionEvent) {
        expression.clear();
        returnValueDouble();

    }

    /**
     * метод для удаления элемента в текстовом поле @code{expression}
     *
     * @param actionEvent событие
     */
    public void clean(ActionEvent actionEvent) {
        cleanError();
        if (!expression.getText().trim().isEmpty() && expression.getText().length() != 1) {
            expression.setText(expression.getText().trim());
            expression.setText(expression.getText().substring(0, expression.getText().length() - 1));
            expression.setText(expression.getText().trim());
            if (expression.getText().charAt(expression.getText().length() - 1) == '+' ||
                    expression.getText().charAt(expression.getText().length() - 1) == '-' ||
                    expression.getText().charAt(expression.getText().length() - 1) == '/' ||
                    expression.getText().charAt(expression.getText().length() - 1) == '*' ||
                    expression.getText().charAt(expression.getText().length() - 1) == '^') {
                expression.setText(expression.getText() + " ");
            }
        } else {
            expression.clear();
        }
        if (expression.getText().indexOf('.') == -1 ||
                expression.getText().lastIndexOf('.') < expression.getText().indexOf("+") ||
                expression.getText().lastIndexOf('.') < expression.getText().indexOf("-") ||
                expression.getText().lastIndexOf('.') < expression.getText().indexOf("/") ||
                expression.getText().lastIndexOf('.') < expression.getText().indexOf("*") ||
                expression.getText().lastIndexOf('.') < expression.getText().indexOf("^")) {
            returnValueDouble();
        } else {
            buttonDouble.setEllipsisString("");
        }
    }

    /**
     * Метод для добавления в текстовое поле @code{expression} отрицательного знака для числа
     *
     * @param actionEvent событие
     */
    public void negative(ActionEvent actionEvent) {
        cleanError();
        if (expression.getText().isEmpty() || expression.getText().charAt(expression.getText().length() - 1) == ' ') {
            expression.setText(expression.getText() + buttonNegative.getEllipsisString());
        }
    }

    /**
     * Метод для проверки длины выражения
     *
     * @param textField текстовое поле
     * @return true если длина выражения < 3 иначе false
     */
    private boolean isValidLength(TextField textField) {
        String[] elements = expression.getText().split(" ");
        return elements.length < 3;
    }

    /**
     * Метод для обозначения дробного числа в текстовом поле @code{expression}
     *
     * @param actionEvent событие
     */
    public void doubleNumber(ActionEvent actionEvent) {
        cleanError();
        expression.setText(expression.getText() + buttonDouble.getEllipsisString());
        buttonDouble.setEllipsisString("");
    }

    /**
     * Метод, возвращающей кнопке @code{buttonDouble} свое значение
     */
    private void returnValueDouble() {
        buttonDouble.setEllipsisString(".");
    }

    /**
     * Метод, добавляющий в текстовое поле @code{expression} цифру
     *
     * @param actionEvent событие
     */
    public void writeNumber(ActionEvent actionEvent) {
        cleanError();
        Button number = (Button) actionEvent.getSource();
        expression.setText(expression.getText() + number.getText());
    }

    /**
     * Метод, добавляющий в текстовое поле @code{expression} знак операнда
     *
     * @param actionEvent событие
     */
    public void writeSign(ActionEvent actionEvent) {
        cleanError();
        if (!expression.getText().isEmpty() && expression.getText().charAt(expression.getText().length() - 1) != ' ' &&
                isValidLength(expression) && expression.getText().charAt(expression.getText().length() - 1) != '-' &&
                expression.getText().charAt(expression.getText().length() - 1) != '.') {
            Button sign = (Button) actionEvent.getSource();
            expression.setText(expression.getText() + " " + sign.getText() + " ");
            returnValueDouble();
        }
    }
}
